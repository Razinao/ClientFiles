# ![logo](http://i.imgur.com/EusH9ql.png)

## What is Paragon WoW?
Paragon WoW is a World of Warcraft RolePlaying server, free to play and for everyone to enjoy.

Paragon WoW has the usage of GM commands for making the life of their roleplayers easier.

## What are the requirements?
Paragon requires can run on various operating systems such as Windows, Linux or Mac (providing you have the required programs to run Legion servers).

## The server runs on Legion Patch 7.3.5 (version 26972).

However, you will have to download and install our custom WoW.exe to play here.

## What Paragon WoW offers you:
- Blizzard supported lore rather than homebrewed.
- A more controlled environment in terms of peoples characters (application system)
- A stable server which can last for days until restarted.
- Massive list of custom items - everything that the game can offer has a custom item.
- A versatile offer in RP styles.